import java.util.Scanner;

public class GitLabExample {
    public static void main(String[] args) {
        System.out.println("Please, enter your name");
        while (true) {
            Scanner sc = new Scanner(System.in);
            String name = sc.nextLine();
            if (name != null & name.length() > 1) {
                System.out.println("Hello " + name + " nice to meet you!!");
                System.exit(0);
            } else {
                System.out.println("Please, enter correct name:");
            }
        }

    }
}
